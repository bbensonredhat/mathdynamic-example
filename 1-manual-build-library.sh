#!/usr/bin/env bash
#
# create object files
gcc -c add.c sub.c mult.c divi.c
# file add.o
# add.o: ELF 64-bit LSB relocatable, x86-64, version 1 (SYSV), not stripped
#
# create object files capable of dynamic library construction (PIC)
gcc -Wall -fPIC -c add.c sub.c mult.c divi.c
# file add.o
# add.o: ELF 64-bit LSB relocatable, x86-64, version 1 (SYSV), not stripped
# ...same as first
#
# create shared library: libmymath.so
gcc -shared -o libmymath.so add.o sub.o mult.o divi.o
# file libmymath.so 
# libmymath.so: ELF 64-bit LSB shared object, x86-64, version 1 (SYSV), dynamically linked, BuildID[sha1]=42cbf4cd5a6f46bbe912db1f22eb5bdffd641593, not stripped
