#!/usr/bin/env bash
#
# removes all built stuff manually
rm *.o
rm *.a
rm *.so
rm ./mathDynamic
sudo rm /usr/lib64/libmymath.so  #be careful, better to have the path in /etc/ld.so.conf.d
