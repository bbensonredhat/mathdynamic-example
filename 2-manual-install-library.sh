#!/usr/bin/env bash
#
#
# possible places linker searches:
# - /etc/ld.so.conf
# - /etc/ld.so.conf.d/*
# - system search path (/lib, /usr/lib)
# - uniq dir extract of ldconfig -p  # can be 2300+ lines
# - $LD_LIBRARY_PATH
# - executable can have the path hardcoded rpath that can be relpaths: DT_RPATH or DTRUNPATH 
# this can vary on various platforms an may need to be adjusted
#LD_DEBUG=libs /lib64/ld-linux-x86-64.so.2 --inhibit-cache /bin/true 2>&1 | sed '/.*search path=/!d;s///;s/[[:blank:]]*([^)]*)$//' | tr : '\n' | sort -u
#
# Install the library
sudo cp libmymath.so /usr/lib64/
sudo ldconfig
#sudo ldconfig -p | grep mymath
